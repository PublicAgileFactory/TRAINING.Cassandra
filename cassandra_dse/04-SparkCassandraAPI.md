

## Read from Cassandra ##

		sc.cassandraTable("killrvideo","users").take(10) foreach println

		 case class User(userid: java.util.UUID, created_date: java.util.Date, email: String, firstname: String, lastname: String)	

		sc.cassandraTable[User]("killrvideo","users").take(10) foreach println

## Save to Cassandra ##

		sc.cassandraTable[User]("killrvideo","users").saveAsCassandraTable("killrvideo","sparksave", SomeColumns("userid","lastname"))

Cassandra table schema will be inferred by RDD record type. SomeColumns specifies only the mapping.
In CQL
	
		select * from killrvideo.sparksave

If you don't want User mapping, you can also proceed in this way

		sc.cassandraTable[User]("killrvideo","users").map(e => (e.firstname, e.lastname)).saveAsCassandraTable("killrvideo","sparksave2")
		
		select * from killrvideo.sparksave2

Not really the best option.

Let's see api

		val users = sc.cassandraTable[User]("killrvideo","users")
users: com.datastax.spark.connector.rdd.CassandraTableScanRDD[User] = CassandraTableScanRDD[0] at RDD at CassandraRDD.scala:15

		users.filter(_.firstname == "Paolo")
res5: org.apache.spark.rdd.RDD[User] = MapPartitionsRDD[3] at filter at <console>:65

If you apply a filter operation get a normal RDD so you can't invoke specific Cassandra methods anymore
Instead you can use Where and Select

		users.where("firstname = Paolo")
res7: com.datastax.spark.connector.rdd.CassandraTableScanRDD[User] = CassandraTableScanRDD[4] at RDD at CassandraRDD.scala:15

Because of is a native method and it generates CQL, you have the same limitations, so you can only filter on primary key and then on clustering columns

		users.where("firstname = Paolo")

Exception !!!!


		users.where("userid = 449e4bc3-e254-4da4-a796-5a6036178155").count


## cassandraCount ##

Now it works, let's see also cassandraCount

		users.where("userid = 449e4bc3-e254-4da4-a796-5a6036178155").cassandraCount

What happens if we try to apply cassandraCount to a non Cassandra specific RDD ?

		users.filter(_.userid.toString == "449e4bc3-e254-4da4-a796-5a6036178155").cassandraCount
<console>:65: error: value cassandraCount is not a member of org.apache.spark.rdd.RDD[User]

Instead the classic spark count is working             

		users.filter(_.userid.toString == "449e4bc3-e254-4da4-a796-5a6036178155").count


## Grouping ##

classical way

		users.groupBy(_.firstname).mapValues(_.size).collectAsMap

But this is triggering full table scan on cassaandra side and full shuffle on spark side


Use spanByKey, because it tries to perform cassandra side aggregation on primary key columns, si it is far better in terms of performances and reduced shuffling.

		users.spanBy(_.firstname).mapValues(_.size).collectAsMap

In this case is not bringing value, because firstname is neither partition key neither clustering column

Let's insert some data

		iinsert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-78c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-78c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-78c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-78c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (d37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-88c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-98c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-98c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-98c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-98c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-98c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);
		insert into comments_by_video ( videoid, commentid, comment, userid ) values (e37d661d-7e61-49ea-96a5-68c34e83db3a,now(),'commento',c37d661d-7e61-49ea-96a5-68c34e83db3a);

Load them in spark 

		val comments_by_video = sc.cassandraTable("killrvideo","comments_by_video").select("videoid","commentid","userid").as((videoid: java.util.UUID,commentid: java.util.UUID,userid: java.util.UUID) => ((videoid,commentid),userid))


		comments_by_video.groupByKey.mapValues(_.size).toDebugString

		(3) MapPartitionsRDD[19] at mapValues at <console>:63 []
		 |  ShuffledRDD[18] at groupByKey at <console>:63 []
		 +-(3) CassandraTableScanRDD[13] at RDD at CassandraRDD.scala:15 []

		
		comments_by_video.spanByKey.mapValues(_.size).toDebugString

		(3) MapPartitionsRDD[17] at mapValues at <console>:63 []
		 |  SpannedByKeyRDD[16] at RDD at SpannedByKeyRDD.scala:13 []
		 |  CassandraTableScanRDD[13] at RDD at CassandraRDD.scala:15 []

As you can see, no more shuffled rdd, shuffling in spanByKey could occurr but it trasparent to the developer, so pay attention.


## Joining ##



		
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (c37d661d-7e61-49ea-96a5-68c34e83db3a,dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (c37d661d-7e61-49ea-96a5-68c34e83db3b,dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (c37d661d-7e61-49ea-96a5-68c34e83db3c,dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (c37d661d-7e61-49ea-96a5-68c34e83db3d,dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (c37d661d-7e61-49ea-96a5-68c34e83db3e,dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());
		insert into videos (videoid, added_date, description, location, location_type, name, preview_image_location, tags, userid) values (uuid(),dateof(now()),'a','',0,'aaa','',NULL,uuid());

Load them in spark

		val videos = sc.cassandraTable("killrvideo","videos")
		videos.joinWithCassandraTable("killrvideo","comments_by_video").collect foreach println

No let's try to join without the partition key

		videos.joinWithCassandraTable("killrvideo","comments_by_video").on(SomeColumns("commentid")).collect foreach println

		java.lang.IllegalArgumentException: requirement failed: Can't join without the full partition key. Missing: [ Set(videoid) ]

Join wants the partition key, otherwise it triggers a full table scan.


In order to efficiently join an RDD with a cassandra table instead we have to force the cassandra aware repartition.


		case class Video2(videoid: java.util.UUID)
		val videos2 = sc.parallelize(Seq(Video2(java.util.UUID.randomUUID()), Video2(java.util.UUID.randomUUID())))

		videos2.repartitionByCassandraReplica("killrvideo","comments_by_video").joinWithCassandraTable("killrvideo","comments_by_video").collect foreach println

We need to define Video2 case class, bacause we need two entities with same key names in order to perform joining



## Spark-SQL ##


		ccm node1 dse spark-sql

		select * from killrvideo.videos;

So in this shell you can try SQL statements

		select * from killrvideo.videos where name = 'bbb';

It's important to see that all statements are allowed in Spark SQL,also those not compliant with CQL.Those will perform full table scan.

Now return on classic spark shell.

		val csc = new CassandraSQLContext(sc)
		csc.sql("select * from killrvideo.videos")

		
		res1: org.apache.spark.sql.DataFrame = [videoid: string, added_date: timestamp, description: string, location: string, location_type: int, name: string, preview_image_location: string, tags: array<string>, userid: string]

We only need to instantiate a CassandraSQLContext and we can start to use Dataframe. As you can see Dataframe can infer the schema from the Cassandra table.

		csc.sql("select * from killrvideo.videos").show()

We can perform any kind of query, without CQL limitations, because CassandraConnector ill understand witch statement can be pushed woth and witch not. 


		csc.sql("select * from killrvideo.videos where videoid = '905cee8d-f160-4d06-acc3-ed2cf682cfc2'").show()
		csc.sql("select * from killrvideo.videos where name = 'bbb'").show()

You can perform all SQL queries you want, but in any case performances will be deeply affected from cassandra data model.
		

		





		
		










		
		





		



		

		

		