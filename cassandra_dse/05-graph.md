## graph ##

		ccm stop

		ccm node1 setworkload graph
		ccm node2 setworkload graph
		ccm node3 setworkload graph

		ccm start

Probably will crash, but nodes are up and running

Otherwise with standalone dse

		nodetool decommission
		service dse stop
		vi /etc/default/dse
		ENABLE_GRAPH=1
		rm -rf /var/lib/cassandra/data/*
		service dse start

		
		
Open the gremlin console


		ccm node1 dse gremlin-console
		
If you want install Datastax Studio you can follow this guide:

		http://docs.datastax.com/en/studio/1.0/studio/gs/installation.html

Let's connect to datastax studio http://127.0.0.1:9091/ to give a look

		
In order to make gremlin-console work, stop all nodes, change in dse.default GRAPH_ENABLED=1 and restart nodes

Inside the gremlin console:

		:clear
		:remote config alias reset
		system.graph('food').create()

		:remote config alias g food.g

		g.V().count()

Creating the schema

		schema.propertyKey('name').Text().ifNotExists().create()        
		schema.propertyKey('gender').Text().create()
		schema.propertyKey('instructions').Text().create()
		schema.propertyKey('category').Text().create()
		schema.propertyKey('year').Int().create()
		schema.propertyKey('timestamp').Timestamp().create()
		schema.propertyKey('ISBN').Text().create()
		schema.propertyKey('calories').Int().create()
		schema.propertyKey('amount').Text().create()
		schema.propertyKey('stars').Int().create()
		// single() is optional, as it is the default
		schema.propertyKey('comment').Text().single().create()
		// Example of a multiple property that can have several values
		// Next 4 lines define two properties, then create a meta-property 'livedIn' on 'country'  
		// A meta-property is a property of a property
		// EX: 'livedIn': '1999-2005' 'country': 'Belgium'     
		// schema.propertyKey('nickname').Text().multiple().create()    
		// schema.propertyKey('country').Text().create()                        
		// schema.propertyKey('livedIn').Text().create()                        
		// schema.propertyKey('country').Text().properties('livedIn').create()  
		    		
		// Vertex Labels
		schema.vertexLabel('author').ifNotExists().create()
		schema.vertexLabel('recipe').create()
		// Example of creating vertex label with properties
		// schema.vertexLabel('recipe').properties('name','instructions').create()
		schema.vertexLabel('ingredient').create()
		schema.vertexLabel('book').create()
		schema.vertexLabel('meal').create()
		schema.vertexLabel('reviewer').create()
		// Example of custom vertex id:
		// schema.propertyKey('city_id').Int().create()
		// schema.propertyKey('sensor_id').Uuid().create()
		// schema().vertexLabel('FridgeSensor').partitionKey('city_id').clusteringKey('sensor_id').create()
		                
		// Edge Labels
		schema.edgeLabel('authored').ifNotExists().create()
		schema.edgeLabel('created').create()
		schema.edgeLabel('includes').create()
		schema.edgeLabel('includedIn').create()
		                
		// Vertex Indexes
		// Secondary
		schema.vertexLabel('author').index('byName').secondary().by('name').add()
		// Materialized	  		
		schema.vertexLabel('recipe').index('byRecipe').materialized().by('name').add()
		schema.vertexLabel('meal').index('byMeal').materialized().by('name').add()
		schema.vertexLabel('ingredient').index('byIngredient').materialized().by('name').add()
		schema.vertexLabel('reviewer').index('byReviewer').materialized().by('name').add()
		// Search
		// schema.vertexLabel('recipe').index('search').search().by('instructions').asText().add()
		// schema.vertexLabel('recipe').index('search').search().by('instructions').asString().add()
		// If more than one property key is search indexed
		// schema.vertexLabel('recipe').index('search').search().by('instructions').asText().by('category').asString().add()
		
		
		// Schema description
		// Use to check that the schema is built as desired
		schema.describe()


Now we can try to add data

		juliaChild = graph.addVertex(label,'author', 'name','Julia Child', 'gender','F')

Schema must be followed

		schema.vertexLabel('author').properties('gender').add()
		schema.describe()


		juliaChild = graph.addVertex(label,'author', 'name','Julia Child', 'gender','F')

		schema.vertexLabel('book').properties('name', 'year', 'ISBN').add()

		frenchChefCookbook = graph.addVertex(label, 'book', 'name', 'The French Chef Cookbook', 'year' , 1968, 'ISBN', '0-394-40135-2')

Now we can try to add some edge

		schema.edgeLabel('authored').connection('author','book').add()
		juliaChild.addEdge('authored', frenchChefCookbook)

		g.V().hasLabel('author').has('name',within('Julia Child')).values('name','gender')

		g.V().hasLabel('author').has('name',within('Julia Child')).out().hasLabel('book').as('Book').values()



	
