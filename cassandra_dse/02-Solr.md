

		ccm node1 setworkload solr

		ccm start


Unfortunately ccm is a little bugged and doesn't support completely DSE.

		ccm node1 nodetool status

Virtual data centers are now visibile


Setup of workload must be done in advance

		ccm remove

Eventually clean gost processes on 9042

		netstat -tulnp
		kill pid


Recreate 

		ccm create --dse -v 5.0.1 --nodes 5 --dse-username **user** --dse-password **pass** dse
		ccm node1 setworkload solr
		ccm node1 setworkload solr
		ccm start

		ccm node1 nodetool ring

Three virtual datacenters

Now let's try to check solr admin console

		http://127.0.0.1:8983/solr/#/

Create a keyspace in Cassandra paying attention to replica factor


		CREATE KEYSPACE IF NOT EXISTS killrvideo WITH REPLICATION = { 'class' : 'NetworkTopologyStrategy', 'Cassandra' : 2, 'Solr' : 1 };
or

		CREATE KEYSPACE IF NOT EXISTS killrvideo WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor': 1 };

		USE killrvideo;
		
		// User credentials, keyed by email address so we can authenticate
		CREATE TABLE IF NOT EXISTS user_credentials (
			email text,
			password text,
			userid uuid,
			PRIMARY KEY (email)
		);
		
		// Users keyed by id
		CREATE TABLE IF NOT EXISTS users (
			userid uuid,
			firstname text,
			lastname text,
			email text,
			created_date timestamp,
			PRIMARY KEY (userid)
		);
		
		// Videos by id
		CREATE TABLE IF NOT EXISTS videos (
			videoid uuid,
			userid uuid,
			name text,
			description text,
			location text,
			location_type int,
			preview_image_location text,
			tags set<text>,
			added_date timestamp,
			PRIMARY KEY (videoid)
		);
		
		// One-to-many from user point of view (lookup table)
		CREATE TABLE IF NOT EXISTS user_videos (
			userid uuid,
			added_date timestamp,
			videoid uuid,
			name text,
			preview_image_location text,
			PRIMARY KEY (userid, added_date, videoid)
		) WITH CLUSTERING ORDER BY (added_date DESC, videoid ASC);
		

Now we create an index, associated to pur table killrvideo.users


		curl "http://localhost:8983/solr/admin/cores?action=CREATE&name=killrvideo.users&generateResources=true&reindex=true"

Let's what happened on web UI.

Creating some data

		insert into killrvideo.users (userid, firstname, lastname, email, created_date) VALUES (uuid(), 'Paolo', 'Platter', 'p.p@agilelab.it', dateOf(now()));
		
		insert into killrvideo.users (userid, firstname, lastname, email, created_date) VALUES (uuid(), 'Pao', 'Platt', 'p.p@agile.it', dateOf(now()));
		insert into killrvideo.users (userid, firstname, lastname, email, created_date) VALUES (uuid(), 'Pao', 'Firpo', 'paolo.p@agilelab.it', dateOf(now()));
		insert into killrvideo.users (userid, firstname, lastname, email, created_date) VALUES (uuid(), 'Alberto', 'Firpo', 'firpoagilelab.it', dateOf(now()));

		select * from killrvideo.users;

As you can see we have a column solr_query, that now we can try to query

		select * from killrvideo.users where email = 'p.p@agilelab.it'

		select * from killrvideo.users where email like '%agilelab%'

No way, but with solr we can

		select * from killrvideo.users where solr_query = 'email:p.p@agilelab.it';

How can we perform something similar to like operator ?

		select * from killrvideo.users where solr_query = 'email:*agilelab*';

We can also test queries from web UI if don't need to mix with cassandra filters


		http://127.0.0.1:8983/solr/#/killrvideo.users/query

Final considerations:
With virtual datacenter you will be able to separate workloads, for example you can pump data into Cassandra DataCenter with Local Quorum consistency, so indexing is not affecting your write latency. Then on Solr datacente you can redirect all your read traffic, specially searches



Now from the interface, we try to update a document

		"userid": "154ae318-4a2c-4440-92ad-a7280f682e6a",
        "firstname": "Paolo",
        "created_date": "2016-11-21T21:43:24.454Z",
        "email": "p.p@databiz.it",
        "lastname": "Platter"

and in cql

		select * from killrvideo.users;








		



		

		

		