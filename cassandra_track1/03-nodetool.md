
		nodetool help

		nodetool status - display cluster state, load, host id and token

		nodetool ring -  aids in comparing load balance and ﬁnding downed nodes

		nodetool info - display node memory use, disk load, uptime, and similar data

Other commands for operation and performance tuning

		nodetool cfstats
		nodetool compact
		nodetool describecluster

Etc.
