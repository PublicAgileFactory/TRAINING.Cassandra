

		CREATE KEYSPACE IF NOT EXISTS internetbanking WITH REPLICATION = { 'class' : 'NetworkTopologyStrategy', 'datacenter1' : 3, 'datacenter2' : 2 };


		
		use internetbanking;
		
		CREATE TABLE IF NOT EXISTS user_credentials (
			username text,
			password text,
			ndg text,
			PRIMARY KEY (username)
		);
		
		CREATE TABLE IF NOT EXISTS user_backoffice_credentials (
			username text,
			password text,
			userid uuid,
			PRIMARY KEY (username)
		);
		
		CREATE TABLE IF NOT EXISTS users (
			ndg text,
			firstname text,
			lastname text,
			created_date timestamp,
			PRIMARY KEY (ndg)
		);
		
		CREATE TABLE IF NOT EXISTS users_backoffice (
			userid uuid,
			firstname text,
			lastname text,
			created_date timestamp,
			PRIMARY KEY (userid)
		);
		
		
		CREATE TABLE IF NOT EXISTS transactions (
			trxid uuid,
			ndg text,
			amount double,
			category text,
			description text,
			date timestamp,
			PRIMARY KEY (trxid)
		);
		
		CREATE TABLE IF NOT EXISTS user_transactions (
			ndg text,
			trxid uuid,
			amount double,
			date timestamp,
			PRIMARY KEY (ndg, date)
		)
		WITH CLUSTERING ORDER BY (
			date DESC);
		
		
		CREATE TABLE IF NOT EXISTS user_transactions_by_year_month_category (
			ndg text,
			year int,
			month int,
			category uuid,
			amount counter,
			PRIMARY KEY ((ndg, year, month),category)
		);
		
		
Find trx ( latest first ) with limit 1000

is a very big problem because we don't have ordering on primary key. We need to find some hedging strategy like the following one.
Without random you could have hotspot, I insert costraint about the user must specify the day in witch he want the latest transactions. In normal situation, application will pre set this condition on "today"


		CREATE TABLE IF NOT EXISTS latest_transactions (
		    yyyymmdd text,
			random int,
			trxid uuid,
			date timestamp,
		    amount double,
		    PRIMARY KEY ((yyyymmdd, random), date,)
		)WITH CLUSTERING ORDER BY (
			date DESC);


		
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12345', 10.0, 'alimentari','supermercato', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12346', 10.0, 'alimentari','supermercato', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12347', 10.0, 'alimentari','supermercato', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12348', 10.0, 'alimentari','supermercato', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12349', 10.0, 'alimentari','supermercato', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12350', 10.0, 'ATM','', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12445', 10.0, 'ATM','', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12545', 10.0, 'ATM','', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12645', 10.0, 'ATM','', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12745', 10.0, 'casa','ikea', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12845', 10.0, 'casa','ikea', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '12945', 10.0, 'casa','ikea', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '13345', 10.0, 'casa','ikea', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '14345', 10.0, 'casa','casanova', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '15345', 10.0, 'casa','casanova', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '16345', 10.0, 'casa','casanova', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '17345', 10.0, 'alimentari','alimentari', dateof(now()));
		insert into transactions ( trxid, ndg, amount, category, description, date ) values ( uuid(), '18345', 10.0, 'alimentari','alimentari', dateof(now()));
		
		
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12346',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12347',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12348',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12349',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12350',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12445',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12545',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12645',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12745',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12845',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '12945',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '13345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '14345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '15345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '16345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '17345',uuid(),  10.0, dateof(now()));
		insert into user_transactions ( ndg,trxid,  amount, date ) values ( '18345',uuid(),  10.0, dateof(now()));
		
		select * from user_transactions where ndg = '12345'
		select * from user_transactions where date >= '2016-10-27 21:52:08.257' ALLOW FILTERING;
		
		
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 12 and category = 'alimentari';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 12 and category = 'alimentari';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 11 and category = 'alimentari';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 11 and category = 'alimentari';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 11 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 11 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '12345' and year = 2016 and month = 12 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 12 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 12 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 12 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 11 and category = 'casa';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 10 and category = 'auto';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 10 and category = 'auto';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 10 and category = 'auto';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 9 and category = 'auto';
		update user_transactions_by_year_month_category  set amount = amount + 12 where ndg = '16345' and year = 2016 and month = 9 and category = 'auto';
		
