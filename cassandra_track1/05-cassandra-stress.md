
		# Insert (write) one million rows
		cassandra-stress write n=1000000 -rate threads=50

		
		# Read two hundred thousand rows.
		cassandra-stress read n=200000 -rate threads=50

		cassandra-stress write n=1 cl=one -mode native cql3 -log file=create_schema.log

		#Run a real write workload
		cassandra-stress write n=1000000 cl=one -mode native cql3 -schema keyspace="keyspace1" -log file=load_1M_rows.log

		nodetool cfstats
		


