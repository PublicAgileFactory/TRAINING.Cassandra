		drop keyspace killrvideo;

		CREATE KEYSPACE IF NOT EXISTS killrvideo WITH REPLICATION = { 'class' : 'NetworkTopologyStrategy', 'datacenter1' : 3 };

		USE killrvideo;



// User credentials, keyed by email address so we can authenticate
This is SIMPLE PRIMARY KEY

		CREATE TABLE IF NOT EXISTS user_credentials (
		    email text,
		    password text,
		    userid uuid,
		    PRIMARY KEY (email)
		);

		insert into user_credentials(email,password,userid) VALUES ('paolo.platter@agilelab.it','pippo',uuid());

		select * from user_credentials where password = 'pippo';

InvalidRequest: code=2200 [Invalid query] message="No supported secondary index found for the non primary key columns restrictions"
With this schema you can only use equal conditions on primary key

		insert into user_credentials(email,password,userid) VALUES ('nicola.breda@agilelab.it','pluto',uuid());

		select * from user_credentials where email = 'paolo.platter@agilelab.it';

		select * from user_credentials where email > 'paolo.platter@agilelab.it';

InvalidRequest: code=2200 [Invalid query] message="Only EQ and IN relation are supported on the partition key (unless you use the token() function)"


COMPOUND PRIMARY KEY

		CREATE TABLE IF NOT EXISTS user_videos (
		    userid uuid,
		    added_date timestamp,
		    videoid uuid,
		    name text,
		    preview_image_location text,
		    PRIMARY KEY (userid, added_date, videoid)
		) WITH CLUSTERING ORDER BY (added_date DESC, videoid ASC);

		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video1','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video3','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video4','');

		select * from user_videos;

		select * from user_videos where videoid = a44a5680-bb6d-4364-a1f5-940d88509de0;

InvalidRequest: code=2200 [Invalid query] message="PRIMARY KEY column "videoid" cannot be restricted as preceding column "added_date" is not restricted"

		select * from user_videos where added_date = '' and videoid = a44a5680-bb6d-4364-a1f5-940d88509de0;

InvalidRequest: code=2200 [Invalid query] message="Cannot execute this query as it might involve data filtering and thus may have unpredictable performance. If you want to execute this query despite the performance unpredictability, use ALLOW FILTERING"

		select * from user_videos where added_date = '' and videoid = a44a5680-bb6d-4364-a1f5-940d88509de0 ALLOW FILTERING


		select * from user_videos where userid in ( 8dc261ef-b0c1-4dda-83ec-7dbfeec99d3e, 117b90ad-a255-4295-b223-7cade4c510d1 ) and added_date > '2016-10-18 22:35:21+0000';

Now filtering is correct. So Primary key with equal comparison, on Clustering columns you can also apply <>, but you need to specify all the keys in the primary key


		drop table user_videos;

		CREATE TABLE IF NOT EXISTS user_videos (
		    userid uuid,
		    added_date timestamp,
		    videoid uuid,
		    name text,
		    preview_image_location text,
		    PRIMARY KEY ((userid, added_date), videoid)
		) WITH CLUSTERING ORDER BY (videoid ASC);


		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video1','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video3','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video4','');

Same query as before

		select * from user_videos where userid in ( 8dc261ef-b0c1-4dda-83ec-7dbfeec99d3e, 117b90ad-a255-4295-b223-7cade4c510d1 ) and added_date > '2016-10-18 22:35:21+0000';

InvalidRequest: code=2200 [Invalid query] message="Only EQ and IN relation are supported on the partition key (unless you use the token() function)"


		select * from user_videos where name = 'video1';

InvalidRequest: code=2200 [Invalid query] message="No supported secondary index found for the non primary key columns restrictions"

Secondary Index

		create index on user_videos (name);

		select * from user_videos where name = 'video1';




