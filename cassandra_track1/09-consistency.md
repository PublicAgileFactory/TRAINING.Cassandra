

		
		
		ccm node3 stop

		ccm node4 stop

		
CONSISTENCY Check
	
		CONSISTENCY;

		select * from isd_weather_data.sky_condition_lookup;

		CONSISTENCY QUORUM;

		select * from isd_weather_data.sky_condition_lookup;

Quorum can't be achieved

Let's see in write mode

		INSERT INTO  isd_weather_data.sky_condition_lookup (code, condition) VALUES (345345, 'EHEH');

		CONSISTENCY ANY;

		INSERT INTO  isd_weather_data.sky_condition_lookup (code, condition) VALUES (34, 'EHEH');
		INSERT INTO  isd_weather_data.sky_condition_lookup (code, condition) VALUES (34456999, 'EHEH');		
		
Some Hints should be present

		cd .ccm/test/node1/hints
		ll

From version 3.0 hints are directly on the file system of the coordinator. In 2.x were in system.hints cassandra table.


Restore nodes and check performance impacts


		DROP KEYSPACE IF EXISTS "Keyspace1"

		cassandra-stress write n=20000 no-warmup -rate threads=1 \ -schema replication\(factor=3\) 

		ccm flush

		cassandra-stress read n=20000 cl=ONE -rate threads=1  

With a CL of ONE, cassandra-stress will read partitions from only one of the replica nodes

		cassandra-stress read n=20000 cl=ALL -rate threads=1 

With a consistency level of ALL, each operation will require all three nodes to read and return the partition. Was the read performance what you expected between the two CLs? If the results are not what you expect, you may want to run the cassandra-stress read again with the CL of ONE for an additional comparison.   

		ccm node3 stop

		ccm node4 stop

Now only node1 and node2 are up. Since the replication factor is 3, this means only 2 of 3 possible replica nodes are available

		cassandra-stress read n=10 cl=ALL no-warmup -rate \ threads=1 -mode thrift 
		cassandra-stress read n=10 cl=QUORUM no-warmup -rate \ threads=1 -mode thrift 

		ccm node2 stop

		cassandra-stress read n=10 cl=QUORUM no-warmup -rate \ threads=1 -mode thrift 

		cassandra-stress read n=10 cl=ONE no-warmup -rate \ threads=1 -mode thrift 






		
