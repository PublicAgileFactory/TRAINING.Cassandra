
		ccm node1 nodetool cfstats -- killrvideo.user_videos

		Keyspace: killrvideo
        Read Count: 0
        Read Latency: NaN ms.
        Write Count: 101
        Write Latency: 0.34460396039603963 ms.
        Pending Flushes: 0
                Table: user_videos
                SSTable count: 1
                Space used (live): 7026
                Space used (total): 7026
                Space used by snapshots (total): 0
                Off heap memory used (total): 90
                SSTable Compression Ratio: 0.7019533711405167
                Number of keys (estimate): 101
                Memtable cell count: 81
                Memtable data size: 6723
                Memtable off heap memory used: 0
                Memtable switch count: 2
                Local read count: 0
                Local read latency: NaN ms
                Local write count: 101
                Local write latency: 0.373 ms
                Pending flushes: 0
                Bloom filter false positives: 0
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 40
                Bloom filter off heap memory used: 32
                Index summary off heap memory used: 42
                Compression metadata off heap memory used: 16
                Compacted partition minimum bytes: 73
                Compacted partition maximum bytes: 86
                Compacted partition mean bytes: 86
                Average live cells per slice (last five minutes): 7.0
                Maximum live cells per slice (last five minutes): 10
                Average tombstones per slice (last five minutes): 1.0
                Maximum tombstones per slice (last five minutes): 1


		describe user_videos;


		CREATE TABLE killrvideo.user_videos (
		    userid uuid,
		    added_date timestamp,
		    videoid uuid,
		    name text,
		    preview_image_location text,
		    PRIMARY KEY ((userid, added_date), videoid)
		) WITH CLUSTERING ORDER BY (videoid ASC)
		    AND bloom_filter_fp_chance = 0.01
		    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
		    AND comment = ''
		    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
		    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
		    AND crc_check_chance = 1.0
		    AND dclocal_read_repair_chance = 0.1
		    AND default_time_to_live = 0
		    AND gc_grace_seconds = 864000
		    AND max_index_interval = 2048
		    AND memtable_flush_period_in_ms = 0
		    AND min_index_interval = 128
		    AND read_repair_chance = 0.0
		    AND speculative_retry = '99PERCENTILE';
		CREATE INDEX user_videos_name_idx ON killrvideo.user_videos (name);


		TRACING ON;

		select * from user_videos where name = 'video1';

		

Far simpler trace if you query partition key

		







		TRACING OFF;



