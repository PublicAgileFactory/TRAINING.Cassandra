

- Check which version of Java is installed by running the following command: 

    	java -version

- Add the DataStax Community repository to the /etc/apt/sources.list.d/cassandra.sources.list

        echo "deb http://www.apache.org/dist/cassandra/debian 311x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list

    
- Add the DataStax repository key to your aptitude trusted keys. 
    
    
		curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -

- Optional 
		sudo apt-key adv --keyserver pool.sks-keyservers.net --recv-key A278B781FE4B2BDA

		

- Install the package. For example: 

		sudo apt-get update
		sudo apt-get install cassandra

- Now the service is up or you can start it as it follows:

		service cassandra start

- Then check

		nodetool status