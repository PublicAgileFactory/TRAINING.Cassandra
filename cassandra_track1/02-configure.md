

- Stop cassandra service

		sudo service cassandra stop 

- Change the cluster name on cassandra.yaml

- restart service

		sudo service cassandra start

- If service doesn't start check log

		tail -f -n1000 /var/log/cassandra/system.log

- Check for exceptions

- Probably you missed to delete data before changing the cluster name

		sudo rm -rf /var/lib/cassandra/data/system/*

- More radical
		sudo rm -rf /var/lib/cassandra/*
