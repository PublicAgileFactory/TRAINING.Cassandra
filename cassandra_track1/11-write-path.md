
		ccm node1 nodetool cfstats -- killrvideo.user_videos



		Keyspace: killrvideo
		        Read Count: 0
		        Read Latency: NaN ms.
		        Write Count: 4
		        Write Latency: 0.1055 ms.
		        Pending Flushes: 0
		                Table: user_videos
		                SSTable count: 1
		                Space used (live): 5514
		                Space used (total): 5514
		                Space used by snapshots (total): 0
		                Off heap memory used (total): 58
		                SSTable Compression Ratio: 0.8110749185667753
		                Number of keys (estimate): 4
		                Memtable cell count: 0
		                Memtable data size: 0
		                Memtable off heap memory used: 0
		                Memtable switch count: 1
		                Local read count: 0
		                Local read latency: NaN ms
		                Local write count: 4
		                Local write latency: 0.111 ms
		                Pending flushes: 0
		                Bloom filter false positives: 0
		                Bloom filter false ratio: 0.00000
		                Bloom filter space used: 16
		                Bloom filter off heap memory used: 8
		                Index summary off heap memory used: 42
		                Compression metadata off heap memory used: 8
		                Compacted partition minimum bytes: 73
		                Compacted partition maximum bytes: 86
		                Compacted partition mean bytes: 86
		                Average live cells per slice (last five minutes): 2.0
		                Maximum live cells per slice (last five minutes): 2
		                Average tombstones per slice (last five minutes): 1.0
		                Maximum tombstones per slice (last five minutes): 1



		use killrvideo;

		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');

		ccm node1 nodetool cfstats -- killrvideo.user_videos

now you can see something in memtable

let's insert some rows more and force the flush of memtables

		ccm node1 nodetool cfstats -- killrvideo.user_videos

		Keyspace: killrvideo
        Read Count: 0
        Read Latency: NaN ms.
        Write Count: 20
        Write Latency: 0.52865 ms.
        Pending Flushes: 0
                Table: user_videos
                SSTable count: 1
                Space used (live): 5514
                Space used (total): 5514
                Space used by snapshots (total): 0
                Off heap memory used (total): 58
                SSTable Compression Ratio: 0.8110749185667753
                Number of keys (estimate): 20
                Memtable cell count: 16
                Memtable data size: 1328
                Memtable off heap memory used: 0
                Memtable switch count: 1
                Local read count: 0
                Local read latency: NaN ms
                Local write count: 20
                Local write latency: 0.566 ms
                Pending flushes: 0
                Bloom filter false positives: 0
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 16
                Bloom filter off heap memory used: 8
                Index summary off heap memory used: 42
                Compression metadata off heap memory used: 8
                Compacted partition minimum bytes: 73
                Compacted partition maximum bytes: 86
                Compacted partition mean bytes: 86
                Average live cells per slice (last five minutes): 2.0
                Maximum live cells per slice (last five minutes): 3
                Average tombstones per slice (last five minutes): 1.0
                Maximum tombstones per slice (last five minutes): 1

flush

		ccm node1 flush

		ccm node1 nodetool cfstats -- killrvideo.user_videos


		Keyspace: killrvideo
        Read Count: 0
        Read Latency: NaN ms.
        Write Count: 20
        Write Latency: 0.52865 ms.
        Pending Flushes: 0
                Table: user_videos
                SSTable count: 2
                Space used (live): 12118
                Space used (total): 12118
                Space used by snapshots (total): 0
                Off heap memory used (total): 132
                SSTable Compression Ratio: 0.7517221982392109
                Number of keys (estimate): 20
                Memtable cell count: 0
                Memtable data size: 0
                Memtable off heap memory used: 0
                Memtable switch count: 2
                Local read count: 0
                Local read latency: NaN ms
                Local write count: 20
                Local write latency: 0.566 ms
                Pending flushes: 0
                Bloom filter false positives: 0
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 48
                Bloom filter off heap memory used: 32
                Index summary off heap memory used: 84
                Compression metadata off heap memory used: 16
                Compacted partition minimum bytes: 73
                Compacted partition maximum bytes: 86
                Compacted partition mean bytes: 86
                Average live cells per slice (last five minutes): 2.0
                Maximum live cells per slice (last five minutes): 3
                Average tombstones per slice (last five minutes): 1.0
                Maximum tombstones per slice (last five minutes): 1

Now we try to understand compaction
Copy and paste mutiple times the following block of inserts.

		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');
		insert into user_videos (userid, added_date, videoid, name, preview_image_location) VALUES (uuid(),toTimestamp(now()),uuid(),'video2','');

		
		Keyspace: killrvideo
        Read Count: 0
        Read Latency: NaN ms.
        Write Count: 101
        Write Latency: 0.34460396039603963 ms.
        Pending Flushes: 0
                Table: user_videos
                SSTable count: 2
                Space used (live): 12118
                Space used (total): 12118
                Space used by snapshots (total): 0
                Off heap memory used (total): 132
                SSTable Compression Ratio: 0.7517221982392109
                Number of keys (estimate): 101
                Memtable cell count: 81
                Memtable data size: 6723
                Memtable off heap memory used: 0
                Memtable switch count: 2
                Local read count: 0
                Local read latency: NaN ms
                Local write count: 101
                Local write latency: 0.373 ms
                Pending flushes: 0
                Bloom filter false positives: 0
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 48
                Bloom filter off heap memory used: 32
                Index summary off heap memory used: 84
                Compression metadata off heap memory used: 16
                Compacted partition minimum bytes: 73
                Compacted partition maximum bytes: 86
                Compacted partition mean bytes: 86
                Average live cells per slice (last five minutes): 7.0
                Maximum live cells per slice (last five minutes): 10
                Average tombstones per slice (last five minutes): 1.0
                Maximum tombstones per slice (last five minutes): 1

		

		ccm node1 compact

		

		Keyspace: killrvideo
        Read Count: 0
        Read Latency: NaN ms.
        Write Count: 101
        Write Latency: 0.34460396039603963 ms.
        Pending Flushes: 0
                Table: user_videos
                SSTable count: 1
                Space used (live): 7026
                Space used (total): 7026
                Space used by snapshots (total): 0
                Off heap memory used (total): 90
                SSTable Compression Ratio: 0.7019533711405167
                Number of keys (estimate): 101
                Memtable cell count: 81
                Memtable data size: 6723
                Memtable off heap memory used: 0
                Memtable switch count: 2
                Local read count: 0
                Local read latency: NaN ms
                Local write count: 101
                Local write latency: 0.373 ms
                Pending flushes: 0
                Bloom filter false positives: 0
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 40
                Bloom filter off heap memory used: 32
                Index summary off heap memory used: 42
                Compression metadata off heap memory used: 16
                Compacted partition minimum bytes: 73
                Compacted partition maximum bytes: 86
                Compacted partition mean bytes: 86
                Average live cells per slice (last five minutes): 7.0
                Maximum live cells per slice (last five minutes): 10
                Average tombstones per slice (last five minutes): 1.0
                Maximum tombstones per slice (last five minutes): 1
