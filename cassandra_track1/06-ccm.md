
		apt-get install git

		apt-get install python-pip python-dev build-essential

		pip install --upgrade pip

		pip install --upgrade virtualenv
		
		apt-get install -y python-pip; sudo pip install cql PyYAML

		git clone https://github.com/pcmanus/ccm.git

		cd ccm; sudo ./setup.py install; cd ..

Edit /etc/hosts  and add loopback interfaces

		127.0.0.1	localhost
		127.0.0.2	localhost
		127.0.0.3	localhost

Start the cluster ( all other cassandra instances must be down ) 
You need to create a user:

useradd -m -s /bin/bash paolo



		ccm create --version 3.11.0 --nodes 3 --start test
		
		ccm list

		ccm status

Now we add a node more

		ccm add node4 -i 127.0.0.4 -j 7400 -b

		ccm status

You have several commands

		ccm --help

		ccm node1 showlog

		ccm node4 start

		ccm status

		ccm node1 ring

		ccm node1 cqlsh

		





