## Performances

Let's start with an easy task

		cassandra-stress write n=200000 cl=one -rate threads=10  -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249,10.1.165.173

		nodetool tpstats

		nodetool cfstats keyspace1.standard1

We have no dropped messages

Now we can try to augment the troughput, starting the following command on each node

		cassandra-stress write n=200000 cl=one -rate threads=100  -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249,10.1.165.173

Dropped mutations? not so easy to overhelm cassandra.

Now let's see how compaction is impacting our latency.
Perform this on each node

		cassandra-stress read n=500000 cl=one -rate threads=400  -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249,10.1.165.173
		
Save results, then perform 
		
		nodetool compact

and repeat the previous test

		cassandra-stress read n=500000 cl=one -rate threads=400  -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249,10.1.165.173

## JVM Monitoring ##

Launch Jvisualvm and connect to remote jmx

In order to accept remote connection to JMX handler add these lines to cassandra-env.sh

		JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl=false"
 		JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.authenticate=false"
 		JVM_OPTS="$JVM_OPTS -Djava.rmi.server.hostname=your ip"

COnnect and launch a stress a test

		cassandra-stress write n=10000000 no-warmup cl=quorum -rate threads=100  -schema "replication(factor=2)" -node 10.1.78.225,10.1.163.243,10.1.163.237,10.1.161.73

And finally we can check gcstats

		nodetool gcstats


		
## cache tuning ##

		cassandra-stress read n=10000000 no-warmup cl=quorum -rate threads=100  -schema "replication(factor=2)" -node 10.1.78.225,10.1.163.243,10.1.163.237,10.1.161.73

		Results:
		Op rate                   :    4,899 op/s  [READ: 4,899 op/s]
		Partition rate            :    4,899 pk/s  [READ: 4,899 pk/s]
		Row rate                  :    4,899 row/s [READ: 4,899 row/s]
		Latency mean              :   20.3 ms [READ: 20.3 ms]
		Latency median            :   13.7 ms [READ: 13.7 ms]
		Latency 95th percentile   :   59.8 ms [READ: 59.8 ms]
		Latency 99th percentile   :  105.8 ms [READ: 105.8 ms]
		Latency 99.9th percentile :  209.2 ms [READ: 209.2 ms]
		Latency max               :  696.8 ms [READ: 696.8 ms]
		Total partitions          :  1,000,000 [READ: 1,000,000]
		Total errors              :          0 [READ: 0]
		Total GC count            : 0
		Total GC memory           : 0.000 KiB
		Total GC time             :    0.0 seconds
		Avg GC time               :    NaN ms
		StdDev GC time            :    0.0 ms
		Total operation time      : 00:03:24

Check performances

		nodetool info
		
		nodetool cfstats keyspace1.standard1

		Keyspace : keyspace1
        Read Count: 780740
        Read Latency: 1.5778790250275379 ms.
        Write Count: 6870834
        Write Latency: 0.04940244401189143 ms.
        Pending Flushes: 0
                Table: standard1
                SSTable count: 9
                Space used (live): 1789447476
                Space used (total): 1789447476
                Space used by snapshots (total): 0
                Off heap memory used (total): 10107042
                SSTable Compression Ratio: 0.0
                Number of keys (estimate): 5320610
                Memtable cell count: 0
                Memtable data size: 0
                Memtable off heap memory used: 0
                Memtable switch count: 25
                Local read count: 780740
                Local read latency: NaN ms
                Local write count: 6870834
                Local write latency: NaN ms
                Pending flushes: 0
                Percent repaired: 4.0
                Bloom filter false positives: 1
                Bloom filter false ratio: 0.00000
                Bloom filter space used: 8896168
                Bloom filter off heap memory used: 8896096
                Index summary off heap memory used: 1210946
                Compression metadata off heap memory used: 0
                Compacted partition minimum bytes: 180
                Compacted partition maximum bytes: 258
                Compacted partition mean bytes: 258
                Average live cells per slice (last five minutes): NaN
                Maximum live cells per slice (last five minutes): 0
                Average tombstones per slice (last five minutes): NaN
                Maximum tombstones per slice (last five minutes): 0
                Dropped Mutations: 0

Now we enable row cache

We edit the cassandra.yaml and we allocate 1024MB for row_cache
then we can alter table to enable it by cql
		
		ALTER TABLE standard1 WITH caching = {'keys': 'ALL', 'rows_per_partition': 'ALL'};

and we repeat the test

		Results:
		Op rate                   :    6,434 op/s  [READ: 6,434 op/s]
		Partition rate            :    6,434 pk/s  [READ: 6,434 pk/s]
		Row rate                  :    6,434 row/s [READ: 6,434 row/s]
		Latency mean              :   15.5 ms [READ: 15.5 ms]
		Latency median            :   11.8 ms [READ: 11.8 ms]
		Latency 95th percentile   :   39.3 ms [READ: 39.3 ms]
		Latency 99th percentile   :   71.0 ms [READ: 71.0 ms]
		Latency 99.9th percentile :  135.3 ms [READ: 135.3 ms]
		Latency max               :  410.3 ms [READ: 410.3 ms]
		Total partitions          :  1,000,000 [READ: 1,000,000]
		Total errors              :          0 [READ: 0]
		Total GC count            : 0
		Total GC memory           : 0.000 KiB
		Total GC time             :    0.0 seconds
		Avg GC time               :    NaN ms
		StdDev GC time            :    0.0 ms
		Total operation time      : 00:02:35

		nodetool info

		Gossip active          : true
		Thrift active          : false
		Native Transport active: true
		Load                   : 1.44 GiB
		Generation No          : 1482279571
		Uptime (seconds)       : 771
		Heap Memory (MB)       : 379.78 / 1946.00
		Off Heap Memory (MB)   : 11.77
		Data Center            : dc1
		Rack                   : rack1
		Exceptions             : 0
		Key Cache              : entries 874914, size 66.75 MiB, capacity 97 MiB, 96 hits, 875186 requests, 0.000 recent hit rate, 14400 save period in seconds
		Row Cache              : entries 341237, size 333.24 KiB, capacity 1 GiB, 704265 hits, 1045503 requests, 0.674 recent hit rate, 0 save period in seconds
		Counter Cache          : entries 0, size 0 bytes, capacity 48 MiB, 0 hits, 0 requests, NaN recent hit rate, 7200 save period in seconds
		Chunk Cache            : entries 18, size 1.12 MiB, capacity 454 MiB, 60 misses, 215 requests, 0.721 recent hit rate, 218.056 microseconds miss latency
		Percent Repaired       : 4.647427633298333%
		Token                  : (invoke with -T/--tokens to see all 256 tokens)



As you can see we have multiple SSTable, but BloomFilter are already doing a pretty good job


