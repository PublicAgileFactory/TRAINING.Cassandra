
Setup a cluster with three nodes up and running.

Populate the cluster with some data using stress-tool

		cassandra-stress write n=1000000 cl=one -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249


## Adding a node ##

Now we add a node

		service cassandra stop

		vi /etc/cassandra/cassandra.yaml

Modify: cluster_name, seed, listen_address, rpc_address, 
auto_boostrap =true (is the default value)
endpoint_snitch = GossipingPropertyFileSnitch


Monitor all other nodes with dstat command and check network activity

Start the fourth node


		rm -rf /var/lib/cassandra/data/*
		rm -rf /var/lib/cassandra/hints/*
		rm -rf /var/lib/cassandra/commitlog/*

		service cassandra start

Once the node joined you can trigger the alignment

		nodetool repair

then we can check the load of nodes

		nodetool status

You can see that load on first three nodes is still the same. This is because streamed partitions are only tombstoned so they are not removed.
Rebalance load on cluster, launching cleanup on the first three nodes.

		nodetool cleanup

Now everything is fine.

## Removing a node ##

Now we simulate a broken node, the number 4.
TO simulate this, we only need to stop node 4

		service cassandra stop

Start again monitoring with
		
		dstat

Now the node is in DOWn state, so we can try to remove it from the node1

		nodetool removenode HOST_ID_NODE4

You can see how node2 and node3 are streaming data owned by node4 to re-align the cluster

		nodetool status

Load quantity is already ok.


## Restoring a downed node ##

If we don't have a recent snapshot, the best way to proceed is to bootstrap the node again, unless we are sure that in the mean time topology didn't change.

		
		
		rm -rf /var/lib/cassandra/data/*
		rm -rf /var/lib/cassandra/hints/*
		rm -rf /var/lib/cassandra/commitlog/*

Go into cassandra.yaml and set

		auto_bootstrap = false

Check load balance

		nodetool status

Rebalance with repair

		nodetool repair

Remember to edit from cassandra.yaml

		auto_bootstrap = true

On all nodes ( this has an impact on performance)  

		nodetool cleanup


## Snapshot ##

On node4

		nodetool snapshot keyspace1

check the snapshot

		ll /var/lib/cassandra/data/keyspace1/standard1*/snapshots/

You can see the hardlinking reporting all the sstables

Let's try to trigger a compaction

		nodetool compact

Now check folders again. Hardlinking is possible only because sstables are immutable


## Restoring a Snaphot ##

Disconnect the node and flush everything

		nodetool drain

Drain disconnect the node from the rpc interface and flush memtables on disk. 
Shutdown the node

		service cassandra stop

Cleanup the commitlog so we are sure that no writes will be replayed once the node will be online again.

		rm -rf /var/lib/cassandra/commitlog/*

Cleanup the data directory, **but pay attention to don't delete snapshot and backup folders**

		cd /var/lib/cassandra/data
		cd keyspace1/standard1*
		rm -rf mc*

Copy the snapshot into the tabe folder

		cp snapshots/snapshotid/* .

Now we perform an other stress command to create an unbalanced situation
On node1

		cassandra-stress write n=10000 cl=one -schema "replication(factor=2)" -node 10.1.160.149,10.1.71.143,10.1.77.249

Restart the node4

		service cassandra start

		nodetool status

In this moment node4 has stale data so we need to run a repair as soon as possible

		nodetool repair
		nodetool status




		

